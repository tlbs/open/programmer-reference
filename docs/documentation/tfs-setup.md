## Setup

- Ensure you are in the **TLBS** local network or connected with VPN if connecting from the outside (e.g. from Solingen)
- If Solution Explorer does not appear in Visual Studio
    - Tools => Options => Source Control => Plug-in Selection => ensure Visual Studio Team Foundation Server is selected in the ComboBox
- Install `TX Text Control.NET for WPF 27.0.exe` & `TX Text Control.NET for WPF 27.0 SP2.exe` from `\\192.168.3.2\Kit\TXTextControl - Full`
    - Use serial key from `sn.txt`
- Make Visual Studio run always as Administrator
    - Right click on VS Shortcut => Properties => Advanced => Check `Run as Administrator`
    - To make Visual Studio always run as administrator, right click on `devenv.exe` => Troubleshoot compatibility => Required additional rights

## Installation
- In Visual Studio, under Team Explorer, click on Connect => Servers => Add
    - In the url field, type `192.168.3.10` => Ok => enter your username and password for the TLBS domain (this is the Romanian TLBS domain. Always prefix the username with TLBS\username)
    - Click on Connect; you should have access to the Team Foundation Server now
- Under Team Explorer, click on `Source Control Explorer`
- Choose a folder on your drive to be the root folder for your sources (e.g. `C:\Programming\TimeLine`). We will name it generically %ROOT%
    - Map `Prototype` to `%ROOT%\Prototype` (right click => map to local folder; confirm with Yes)
    - Map `Release\XX.X` to `%ROOT%\Release\XX.X` for any release version that you might need
- Open the solution from `%ROOT%\Source\TimeLine.sln`
    - Make sure you run Visual Studio as Administrator. It is necessary when building projects like TimeLine.Framework, which have to be registered in COM
- Set the `TLX` project as startup project
- Build whole solution first, ensure that the build succeeds
- Run startup project
